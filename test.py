from app import app
import unittest
from ShoppingListController import *
# ShoppingList = ShoppingListController
from ShoppingListModel import *

class FlaskTestCase(unittest.TestCase):

    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='application/json')
        self.assertEqual(response.status_code, 200)

    def test_get_shopping_list(self):
        tester = app.test_client(self)
        response = tester.get('/get/shopping/list', content_type='application/json')
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
        unittest.main()