from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import json
from settings import app
          
db = SQLAlchemy(app)
class item(db.Model):
    __tablename__ = "item"
    id = db.Column(db.Integer, primary_key=True)
    iName = db.Column(db.String(80), nullable=False)
    iQuantity = db.Column(db.Integer, nullable=False)
    shoppinglist_id = db.Column(db.Integer, db.ForeignKey('shoppinglist.id'), nullable=False)

    # add item to a shopping list
    def add_item(_name, _quantity, _shoppingListID):
       new_item = item(iName=_name, iQuantity=_quantity, shoppinglist_id=_shoppingListID)
       db.session.add(new_item)
       db.session.commit()
