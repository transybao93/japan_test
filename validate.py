class validate():
    def valid_create_shopping_list(request_values):
        if "title" in request_values and "storeName" in request_values:
            return True
        else:
            return False
    
    def valid_update_shopping_list(request_values):
        if "title" in request_values or "storeName" in request_values:
            return True
        else:
            return False

    def valid_create_item(request_values):
        if "name" in request_values and "quantity" in request_values:
            return True
        else:
            return False