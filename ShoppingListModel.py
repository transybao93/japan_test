from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
import json
import datetime
from settings import app

db = SQLAlchemy(app)

# association_table = db.Table('shoppinglist_item',
#     db.Column('shoppinglist_id', db.Integer, db.ForeignKey('shoppinglist.id'), primary_key=True),
#     db.Column('item_id', db.Integer, db.ForeignKey('item.id'), primary_key=True)
# )

class ShoppingList(db.Model):
    __tablename__ = "shoppinglist"
    id = db.Column(db.Integer, primary_key=True)
    slTitle = db.Column(db.String(80), nullable=True)
    slStoreName = db.Column(db.String(80), nullable=True)
    createdAt = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    items = db.relationship('Item', backref='shoppinglist', lazy=True)
    # items = db.relationship('Item', secondary=association_table, lazy='subquery', backref=db.backref('shoppinglist', lazy=True))

    def json(self):
        return_value = {
            'id': self.id,
            'title': self.slTitle, 
            'name': self.slStoreName,
            # 'items': self.items
            'items': [Item.json(item) for item in Item.query.filter_by(shoppinglist_id = self.id).all()]
        }

        return return_value


class Item(db.Model):
    __tablename__ = "item"
    id = db.Column(db.Integer, primary_key=True)
    iName = db.Column(db.String(80), nullable=False)
    iQuantity = db.Column(db.Integer, nullable=False)
    createdAt = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    shoppinglist_id = db.Column(db.Integer, db.ForeignKey('shoppinglist.id'), nullable=False)

    def json(self):
        return {
            'id': self.id,
            'item name': self.iName, 
            'quantity': self.iQuantity,
            'shoppinglist_id': self.shoppinglist_id
            # 'shoppinglist_id': [ShoppingList.json(shoppinglist) for shoppinglist in ShoppingList.query.filter(ShoppingList.items.id==self.id).all()]
        }