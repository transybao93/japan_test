from ShoppingListModel import ShoppingList, db, Item, json
class ShoppingListController():

    def add_shopping_list(_title, _storeName):
        new_shopping_list = ShoppingList(slTitle=_title, slStoreName=_storeName)
        db.session.add(new_shopping_list)
        db.session.commit()

    def get_all_shopping_list():
        return [ShoppingList.json(shoppinglist) for shoppinglist in ShoppingList.query.all()]

    def get_shopping_list_by_title(_title):
        return [ShoppingList.json(shoppinglist) for shoppinglist in ShoppingList.query.filter_by(slTitle=_title).all()]

    def get_shopping_list_like_title(_title):
        return [ShoppingList.json(shoppinglist) for shoppinglist in ShoppingList.query.filter(ShoppingList.slTitle.like('%' + _title + '%')).all()]

    def __repr__(self):
        shoppingListObject = {
            'id': ShoppingList.id,
            'title': ShoppingList.slTitle, 
            'name': ShoppingList.slStoreName,
            'createdAt': ShoppingList.createdAt
        }
        return json.dumps(shoppingListObject)

    def update_shopping_list(_shoppingListID, data):

        sl = ShoppingList.query.filter_by(id=_shoppingListID).first()
        for x in data:
            if x == "storeName":
                sl.slStoreName = data[x]
            if x == "title":
                sl.slTitle = data[x]
        db.session.commit()
        return [ShoppingList.json(sl)]
    
    def delete_shopping_list(_shoppingListID):
        ShoppingList.query.filter_by(id=_shoppingListID).delete()
        Item.query.filter_by(shoppinglist_id=_shoppingListID).delete()
        db.session.commit()
