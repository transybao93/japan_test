from flask import Flask, jsonify, request, Response
# from ShoppingListModel import *
from settings import *
import logging
from logging.handlers import RotatingFileHandler
from validate import *
from ShoppingListController import *
from ItemController import *
ShoppingList = ShoppingListController
Item = ItemController

@app.route('/')
def hello():
    return jsonify({'message': 'hello world !'}), 200

# shopping list router
@app.route('/get/shopping/list')
def getShoppingList():
    slist = ShoppingList.get_all_shopping_list()
    return jsonify({"shoppinglist": slist}), 200

@app.route('/get/shopping/list/by/title/<string:title>')
def get_shopping_list_by_title(title):
    print(title)
    slist = ShoppingList.get_shopping_list_by_title(title)
    return jsonify({
        "shopping list": slist
    }), 200

@app.route('/get/shopping/list/by/item/name/<string:itemName>')
def get_shopping_list_by_item_name(itemName):
    print(itemName)
    slist = Item.get_item_by_name(itemName)
    return jsonify({
        "shopping list": slist
    }), 200

# def valid_create_shopping_list(request_values):
#     if "title" in request_values and "storeName" in request_values:
#         return True
#     else:
#         return False

@app.route('/shopping/list/create', methods=['POST'])
def add_new_shopping_list():
    request_data = request.values

    if validate.valid_create_shopping_list(request.values):
        # print(True)
        ShoppingList.add_shopping_list(request_data['title'], request_data['storeName'])
        return jsonify({
            "message": "Created new shopping list"
        }), 201
    else:
        # print(False)
        return jsonify({
            "message": "Invalid input data, please check again"
        }), 403
        

    # ShoppingList.add_shopping_list(request_data['title'], request_data['storeName'])
    return jsonify({
        "message": "Created new shopping list !"
    }), 201

# def valid_update_shopping_list(request_values):
#     if "title" in request_values or "storeName" in request_values:
#         return True
#     else:
#         return False

@app.route("/shopping/list/update/<int:shoppingListID>", methods=['PUT'])
def update_shopping_list(shoppingListID):
    
    # create new data
    data = {}
    if "storeName" in request.values:
        data['storeName'] = request.values['storeName']
    if "title" in request.values:
        data['title'] = request.values['title']

    if validate.valid_update_shopping_list(request.values):
        # print(True)
        
        # create new shopping list
        # log and call function
        # app.logger.info('shopping list id: %d', shoppingListID)
        # app.logger.info(request.values)
        sl = ShoppingList.update_shopping_list(shoppingListID, data)
        
        return jsonify({
            "message": "Update shopping list success",
            "updated_shopping_list": sl
        }), 200

    else:
        # print(False)
        return jsonify({
            "message": "Invalid input data, please check again"
        }), 403

    

@app.route('/shopping/list/delete/<int:shoppingListID>', methods=['DELETE'])
def delete_shopping_list(shoppingListID):
    ShoppingList.delete_shopping_list(shoppingListID)
    return jsonify({
        "message": "Delete success"
    }), 200

# item router

# def valid_create_item(request_values):
#     if "name" in request_values and "quantity" in request_values:
#         return True
#     else:
#         return False

@app.route("/item/create/<int:shoppingListID>", methods=['POST'])
def add_new_item(shoppingListID):
    request_data = request.values
    if validate.valid_create_item(request.values):
        # print(True)

        # create new item
        new_item = Item.add_item(request_data['name'], request_data['quantity'], shoppingListID)
        return jsonify({
            "message": "Created new item",
            "new_or_updated_item": new_item
        }), 201
    else:
        # print(False)
        return jsonify({
            "message": "Invalid input data, please check again"
        }), 403
    
@app.route('/get/items')
def get_all_item():
    ilist = Item.get_all_item()
    return jsonify({
        "items": ilist
    }), 200

@app.route('/get/item/<int:id>')
def get_item_by_id(id):
    return jsonify({
        "items": Item.get_item_by_id(id)
    }), 200


if __name__ == "__main__":
    # initialize the log handler
    logHandler = RotatingFileHandler('info.log', maxBytes=1000, backupCount=1)
    
    # set the log handler level
    logHandler.setLevel(logging.INFO)

    # set the app logger level
    app.logger.setLevel(logging.INFO)

    app.logger.addHandler(logHandler)    
    app.run(port=5000)
