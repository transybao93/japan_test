from ShoppingListModel import Item, db

class ItemController():
    # add item to a shopping list - POST
    def add_item(_name, _quantity, _shoppingListID):
        
        # check if same name and shopping list id
        # then increase the quantity in database
        # of old items
        return_value = None
        itemCount = Item.query.filter(Item.iName == _name).filter(Item.shoppinglist_id == _shoppingListID).count()
        if itemCount == 0:
            new_item = Item(iName=_name, iQuantity=_quantity, shoppinglist_id=_shoppingListID)
            db.session.add(new_item)
            db.session.commit()
            return_value = Item.json(new_item)
        else:
            print('update quantity..')
            # increase quantity
            oldItem = Item.query.filter(Item.iName == _name).filter(Item.shoppinglist_id == _shoppingListID).first()
            oldItem.iQuantity = oldItem.iQuantity + int(_quantity)
            db.session.commit()
            
            return_value = Item.json(oldItem)
        return return_value
    
    def get_all_item():
        return [Item.json(item) for item in Item.query.all()]

    def get_item_by_name(_itemName):
        # get shoppinglist_id from item
        item = Item.query.filter_by(iName=_itemName).all()
        ids = []
        for x in item:
            ids.append(x.shoppinglist_id)
        sl = ShoppingList.query.filter(ShoppingList.id.in_(ids)).with_entities(ShoppingList.id, ShoppingList.slTitle, ShoppingList.slStoreName).all()
        # print(ids)
        # print(sl)
        return [ShoppingList.json(shoppinglist) for shoppinglist in sl]
    
    def get_item_by_id(_itemID):
        i = Item.query.filter_by(id=_itemID).all()
        return [Item.json(item) for item in i]